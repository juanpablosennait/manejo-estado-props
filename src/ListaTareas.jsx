import React from "react";

const ListaTareas = props => {
  const { tareas, agregarTarea, eliminarTarea } = props;

  const mostrarTareas = () => {
    return tareas.map((tareaElemento, indice) => {
      return (
        <p
          key={indice}
          onClick={() => {
            agregarTarea({
              id: tareas[tareas.length - 1].id + 1,
              tarea: `Tarea ${tareas[tareas.length - 1].id + 1}`
            });
          }}
        >
          {tareaElemento.tarea}
        </p>
      );
    });
  };

  return <div>{mostrarTareas()}</div>;
};

export default ListaTareas;
