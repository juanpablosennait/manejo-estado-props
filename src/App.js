import "./App.css";

import React, { Component } from "react";

import logo from "./logo.svg";
import ListaTareas from "./ListaTareas";

export default class App extends Component {
  state = {
    tareas: [{ id: 0, tarea: "Enseñar reactjs" }, { id: 1, tarea: "Enseñar django" }]
  };

  agregarTarea = (tarea) => {
    let nuevasTareas = this.state.tareas;
    nuevasTareas.push(tarea);
    this.setState({ tareas: nuevasTareas }, () => {
      console.log(this.state.tareas);
    });
  }

  eliminarTarea = (indice) => {
    let nuevasTareas = this.state.tareas;
    nuevasTareas.splice(indice, 1);
    this.setState({ tareas: nuevasTareas }, () => {
      console.log(this.state.tareas)
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <ListaTareas tareas={this.state.tareas} agregarTarea={this.agregarTarea} eliminarTarea={this.eliminarTarea}></ListaTareas>
      </div>
    );
  }
}
